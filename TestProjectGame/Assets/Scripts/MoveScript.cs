﻿using UnityEngine;
using System.Collections;

public class MoveScript : MonoBehaviour {



    public GameObject Player;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {


        if (Input.GetKey(KeyCode.UpArrow))
        {

            Player.transform.Translate(new Vector3(0,0.1f,0));
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {

            Player.transform.Translate(new Vector3(0, -0.1f, 0));
        }



    }
}
